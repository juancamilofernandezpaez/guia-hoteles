[33mcommit ba5774ed7c45128b91ca5a0aaff222f9288ed545[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31morigin/master[m[33m)[m
Author: Juan Camilo Fernández Páez <juancamilofernandezpaez@gmail.com>
Date:   Thu Aug 6 20:38:41 2020 -0500

    Implementamos el uso de formularios

[33mcommit 4a49b46b6a4190a6aac0e73ee49858f3543cd879[m
Author: Juan Camilo Fernández Páez <juancamilofernandezpaez@gmail.com>
Date:   Wed Aug 5 18:16:57 2020 -0500

    Usando open icons

[33mcommit d2b46f023360dd1a04ffb240d1266fb8aeabcde4[m
Author: Juan Camilo Fernández Páez <juancamilofernandezpaez@gmail.com>
Date:   Tue Aug 4 21:16:53 2020 -0500

    Agregamos navegabilidad

[33mcommit 1d37c2e53a4fbe78b4353963ed0c97541fc13830[m
Author: Juan Camilo Fernández Páez <juancamilofernandezpaez@gmail.com>
Date:   Sun Aug 2 18:53:45 2020 -0500

    Uso de librería flex y uso de css

[33mcommit c8482199b6bcde0621699d190b30f29244c9ea49[m
Author: Juan Camilo Fernández Páez <juancamilofernandezpaez@gmail.com>
Date:   Sun Aug 2 11:47:35 2020 -0500

    Inclusión botón en index.html

[33mcommit 478a5fbc675e20a36fac7daf73ba64babf6c4649[m
Author: Juan Camilo Fernández Páez <juancamilofernandezpaez@gmail.com>
Date:   Sat Aug 1 19:00:32 2020 -0500

    Ejercicio clase grillas en index.html

[33mcommit ee6d6f0a6d31b3a73cf762b86ae0b6f6757a2b7d[m
Author: Juan Camilo Fernández Páez <juancamilofernandezpaez@gmail.com>
Date:   Fri Jul 31 17:43:40 2020 -0500

    Incluimos bootstrap

[33mcommit be7dbb451c4ae9205ef6cbc2fae99f3b0fb1eb93[m
Author: Juan Camilo Fernández Páez <juancamilofernandezpaez@gmail.com>
Date:   Thu Jul 30 21:47:11 2020 -0500

    Incluimos index.html

[33mcommit e6b0a78d7401bf275e63449df1068e105a8d055a[m
Author: Juan Camilo Fernández Páez <juancamilofernandezpaez@gmail.com>
Date:   Thu Jul 30 21:28:34 2020 -0500

    Incluimos lite-server

[33mcommit 87eb93e0e9060b9f19aa5fa13c062a33bb48ad6c[m
Author: Juan Camilo Fernández Páez <juancamilofernandezpaez@gmail.com>
Date:   Thu Jul 30 20:37:48 2020 -0500

    Definicion del package.json

[33mcommit 36684779fc1b2d47c6038ba064031b288fd5485e[m
Author: Juan Camilo Fernández Páez <juancamilofernandezpaez@gmail.com>
Date:   Sun Jul 26 18:00:03 2020 -0500

    creacion inicial del proyecto con un readme
